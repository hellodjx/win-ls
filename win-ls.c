/*
项目名：win-ls
作者：邓键祥（使用到的dbg.h,list.h,list.c的原作者为Zed A.Shaw）
许可协议：GPLv3
*/

#include "file_info.h"

//遍历设置显示选项的命令行参数并根据参数设置TotalFilesInfo结构体
int switch_args(TotalFilesInfo *total_files_info, char const *argv[], int where)
{
    for(int i = 1; argv[where][i]!='\0'; i++){
        switch(argv[where][i]){
            case 'C': 
                total_files_info->format = SHORT_FORMAT;
                break;
            case 'l': 
                total_files_info->format = LONG_LIST_FORMAT;
                break;
            case 'a': 
                unset_ignore_setting(total_files_info, Ignore_Hidden);
                break;
            case 'r': 
                total_files_info->sort = SORT_BY_REVERSAL_FILENAME;
                break;
            case 't': 
                total_files_info->sort = SORT_BY_LAST_EDIT_TIME;
                break;
            default: 
                sentinel("The arg %c is not support.", argv[where][i]);
        }
    }

    return 0;
error:
    return -1;
}

//按照设置输出文件列表
int print_result(TotalFilesInfo *total_files_info)
{
    int res;
 
    if (total_files_info->format==SHORT_FORMAT){                            //默认C参数的显示格式就是以空格隔开文件名
        LIST_FOREACH(total_files_info->files_list, first, next, cur){           //遍历file_list
            struct _finddata_t *fileinfo = (struct _finddata_t *)cur->value;    //将单个的文件信息逐个从list中提取出来
            //在没有-a参数的情况下只打印非隐藏文件的信息，有-a参数的前提下打印全部文件信息
            if (fileinfo->attrib!=34 || !is_set_ignore_setting(total_files_info, Ignore_Hidden)){
                printf("%s\t", fileinfo->name);
            }
        }
    }
    //l参数的显示格式就是每个文件名独自一行显示，并且显示详细信息，格式为：文件类型，文件名，文件大小，修改时间，下面实现-l参数的作用
    else if (total_files_info->format==LONG_LIST_FORMAT){                   
        char file_name[MAX_FILENAME_LEN + 1];                              //file_name与file_tpye字符串方便接下来进行格式化输出
        char file_tpye;

        printf("Total:%d\n", List_count(total_files_info->files_list));
        
        LIST_FOREACH(total_files_info->files_list, first, next, cur){
            struct _finddata_t *fileinfo = (struct _finddata_t *)cur->value;
            if (strlen(fileinfo->name) > MAX_FILENAME_LEN){                 //当文件名长度大于MAX_FILENAME_LEN时将只显示MAX_FILENAME_LEN-3长度的文件名加...
                strncpy(file_name, fileinfo->name, MAX_FILENAME_LEN - 3);
                file_name[MAX_FILENAME_LEN - 3] = '.';
                file_name[MAX_FILENAME_LEN - 2] = '.';
                file_name[MAX_FILENAME_LEN - 1] = '.';
            }
            else{                                                           //文件名长度小于MAX_FILENAME_LEN时将其长度扩增到MAX_FILENAME_LEN
                strcpy(file_name, fileinfo->name);
                for(int j = 0; j < MAX_FILENAME_LEN - strlen(fileinfo->name); j++){
                    strcat(file_name, " ");
                }
            }

            if (fileinfo->attrib==_A_SUBDIR) file_tpye = 'd';               //判断是否为文件夹
            else file_tpye = '~';

            struct tm *tmp_time = localtime(&fileinfo->time_write);         //将time_write时间戳转化为本地时间，存储到结构体tmp_time
            char last_edit_time[50];
            strftime(last_edit_time, sizeof(last_edit_time), "%Y%m%d %H:%M:%S", tmp_time);     //将结构体tmp_time的时间按第三个参数规定的格式转化为字符串

            if (fileinfo->attrib!=34 || !is_set_ignore_setting(total_files_info, Ignore_Hidden)){
                printf("%c    %s    % 12d    %s\n", file_tpye, file_name, fileinfo->size, last_edit_time);
            }
        }
    }

    return 0;
error:
    return -1;
}


int main(int argc, char const *argv[])
{
    int res;
    char *path;

    TotalFilesInfo *info = total_files_info_create();                       //为结构体info申请内存
    check(info!=NULL, "Create info failed.");
    res = init_total_files_info(info);                                      //初始化设置
    check(res==0, "Init info failed.");
    
    //根据参数数量进行对命令行参数的解析，注意第一个命令行参数时程序名字，所以用户附加的第一个参数实际上是第二个参数
    if (argc==1){                                                           //直接使用win-ls
        check((path = getcwd(NULL, 0))!=NULL, "Getcwd error.");             //getcwd函数获取当前工作目录的路径
    }
    else if (argc>=2){                                                      //win-ls后有附加参数，有可能是路径，也有可能是设置显示选项的参数
        if ((int)argv[1][0]!='-'){
            path = argv[1]; 
            for(int i = 2; i < argc; i++){
                res = switch_args(info, argv, i);
                check(res==0, "Switch args failed.");
            }
        }
        else if ((int)argv[1][0]=='-'){
            check((path = getcwd(NULL, 0))!=NULL, "Getcwd error.");
            for(int i = 1; i < argc; i++){
                res = switch_args(info, argv, i);
                check(res==0, "Switch args failed.");
            }
        }
        else{
            sentinel("Usage:win-ls [path][args]");
        }
    }

    strcat(path, "/");          //在用户输入的路径末尾添加/，确保这是路径

    res = find_all_files(info, path);                                   //根据路径查找所有文件/文件夹
    check(res==0, "Can not find anything.");
    res = print_result(info);                                           //输出文件/文件夹列表
    check(res==0, "Print result failed.");
    total_files_info_destory(info);                                     //释放内存

    return 0;

error:
    total_files_info_destory(info);
    return -1;
}