#include "file_info.h"

TotalFilesInfo* total_files_info_create()
{
    return calloc(1, sizeof(TotalFilesInfo));
}

int init_total_files_info(TotalFilesInfo *total_files_info)
{
    //为TotalFilesInfo结构体里的files_list结构体申请内存
    total_files_info->files_list = List_create();
    check(total_files_info->files_list!=NULL, "Can not create List.");

    for(int i = 0; i < MAX_IGNORE_SETTING_NUM; i++){
        total_files_info->ignore_setting[i] = Ignore_None;
    }

    
    total_files_info->ignore_setting[0] = Ignore_Hidden;                //设置不显示隐藏文件
    total_files_info->format = SHORT_FORMAT;                            //设置以简短形式显示文件信息
    total_files_info->sort = SORT_BY_DEFAULT;                           //设置排列方式为默认 

    return 0;

error:
    return -1;
}

void set_ignore_setting(TotalFilesInfo *total_files_info, IgnoreSetting setting)
{
    for(int i = 0; i < MAX_IGNORE_SETTING_NUM; i++){
        if (total_files_info->ignore_setting[i]==Ignore_None){
            total_files_info->ignore_setting[i] = setting;
            break;
        }
    }
}

void unset_ignore_setting(TotalFilesInfo *total_files_info, IgnoreSetting setting)
{
    for(int i = 0; i < MAX_IGNORE_SETTING_NUM; i++){
        if (total_files_info->ignore_setting[i]==setting){
            total_files_info->ignore_setting[i] = Ignore_None;
            break;
        }
    }
}

int find_all_files(TotalFilesInfo *total_files_info, char *path)
{
    struct _finddata_t fileinfo;                    //文件存储信息结构体 
    long fHandle;                                   //保存文件句柄 
    int i = 0;
    struct _finddata_t copy_fileinfo[MAX_FILES_NUM];

    check((fHandle=_findfirst(strcat(path, "*"), &fileinfo))!=-1L, "No such dir.");

    do{
        copy_fileinfo[i].attrib = fileinfo.attrib;
        strcpy(copy_fileinfo[i].name, fileinfo.name);
        copy_fileinfo[i].size = fileinfo.size;
        copy_fileinfo[i].time_access = fileinfo.time_access;
        copy_fileinfo[i].time_create = fileinfo.time_create;
        copy_fileinfo[i].time_write = fileinfo.time_write;
        List_push(total_files_info->files_list, &copy_fileinfo[i]);
        i++;
    }while( _findnext(fHandle, &fileinfo)==0);
     
    _findclose(fHandle);                          //关闭文件

    return 0;

error:
    return -1;
}

int files_list_bubble_sort(List *list, int how_to_sort)
{
    check(check_list_first(list)!=-1, "List's first should not be NULL");
    check(how_to_sort==SORT_BY_DEFAULT || how_to_sort==SORT_BY_LAST_EDIT_TIME ||
    how_to_sort==SORT_BY_REVERSAL_FILENAME, "Not support this sort: %d", how_to_sort);

    int sorted = 1;
    struct _finddata_t *fileinfo;
    struct _finddata_t *next_fileinfo;

    if(List_count(list) <= 1){
        return 0; // already sorted
    }

    if (how_to_sort==SORT_BY_LAST_EDIT_TIME){
        do {
            sorted = 1;
            LIST_FOREACH(list, first, next, cur) {
                if(cur->next) {
                    fileinfo = (struct _finddata_t *)cur->value;
                    next_fileinfo = (struct _finddata_t *)cur->next->value;
                    if(fileinfo->time_write < next_fileinfo->time_write){
                        ListNode_swap(cur, cur->next);
                        sorted = 0;
                    }
                }
            }
        } while(!sorted);
    }

    else if (how_to_sort==SORT_BY_REVERSAL_FILENAME){
        void *arr[MAX_FILES_NUM];
        int i = 0;
        void *temp;
        int node_count = List_count(list);
        ListNode *cur_node, *next_node;
        //将list的元素转存到arr数组中
        LIST_FOREACH(list, first, next, cur){
            arr[i] = cur->value;
            i++;
        }
        //清空file_slist
        for(cur_node = list->first; cur_node!=NULL; ){
            next_node = cur_node->next;
            List_remove(list, cur_node);
            cur_node = next_node;
        }
        //将arr数组中的元素反转
        for(int j = 0; j <= node_count / 2 - 1; j++){
            temp = arr[j];
            arr[j] = arr[node_count - j - 1];
            arr[node_count - j - 1] = temp;
        }
        //将反转后的arr数组元素放回files_list
        for(int j = 0; j < node_count; j++){
            List_push(list, arr[j]);
        }
    }

    return 0;
error:
    return -1;
}

int is_set_ignore_setting(TotalFilesInfo *total_files_info, IgnoreSetting setting)
{
    for(int i = 0; i < MAX_IGNORE_SETTING_NUM; i++){
        if (total_files_info->ignore_setting[i]==setting){
            return 1;
        }
    }

    return 0;
}

void total_files_info_destory(TotalFilesInfo *total_files_info)
{
    List_destroy(total_files_info->files_list);
    free(total_files_info);
}