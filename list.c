#include "list.h"
#include "dbg.h"

List *List_create()
{
    return calloc(1, sizeof(List));
}

void List_destroy(List *list)
{
    LIST_FOREACH(list, first, next, cur){
        if(cur->prev){
            free(cur->prev);
        }
    }

    free(list->last);
    free(list);
}

void List_clear(List *list)
{
    LIST_FOREACH(list, first, next, cur){
        free(cur->value);
    }
}

void List_clear_destroy(List *list)
{
    LIST_FOREACH(list, first, next, cur){
        free(cur->value);
        if(cur->prev){
            free(cur->prev);
        }
    }
}

void List_push(List *list, void *value)
{
    check(list->count>=0, "Count could not less than 0.");
    check(check_list_first(list)!=-1, "List's first should not be NULL");
    ListNode *node = calloc(1, sizeof(ListNode));
    check_mem(node);

    node->value = value;

    if(list->last==NULL){
        list->first = node;
        list->last = node;
    }
    else{
        list->last->next = node;
        node->prev = list->last;
        list->last = node;
    }

    list->count++;

error:
    return;
}

void List_unshift(List *list, void *value)
{
    check(list->count>=0, "Count could not less than 0.");
    check(check_list_first(list)!=-1, "List's first should not be NULL");
    ListNode *node = calloc(1, sizeof(ListNode));
    check_mem(node);

    node->value = value;

    if(list->first==NULL){
        list->first = node;
        list->last = node;
    }
    else{
        node->next = list->first;
        list->first->prev = node;
        list->first = node;
    }

    list->count++;

error:
    return;
}

void *List_remove(List *list, ListNode *node)
{
    void *result = NULL;

    check(list->count>=0, "Count could not less than 0.");
    check(list->first && list->last, "List is empty.");
    check(node, "node can't be NULL");

    if(node==list->first && node==list->last){
        list->first = NULL;
        list->last = NULL;
    }
    else if(node==list->first){
        list->first = node->next;
        check(list->first!=NULL, "Invalid list, somehow got a first that is NULL.");
        list->first->prev = NULL;
    }
    else if(node==list->last){
        list->last = node->prev;
        check(list->last!=NULL, "Invalid list, somehow got a next that is NULL.");
        list->last->next = NULL;
    }
    else{
        ListNode *after = node->next;
        ListNode *before = node->prev;
        after->prev = before;
        before->next = after;
    }

    list->count--;
    result = node->value;
    free(node);

error:
    return result;
}

void *List_pop(List *list)
{
    check(check_list_first(list)!=-1, "List's first should not be NULL");
    check(list->count>=0, "Count could not less than 0.");
    ListNode *node = list->last;
    return node != NULL ? List_remove(list, node) : NULL;
error:
    return NULL;
}

void *List_shift(List *list)
{
    check(check_list_first(list)!=-1, "List's first should not be NULL");
    check(list->count>=0, "Count could not less than 0.");
    ListNode *node = list->first;
    return node!=NULL ? List_remove(list, node) : NULL;
error:
    return NULL;
}

int check_list_first(List *list)
{
    if(list->count>0 && list->first==NULL){
        return -1;
    }

    return 0;
}

void List_copy(List *tolist, List *fromlist)
{
    LIST_FOREACH(fromlist, first, next, cur){
        List_push(tolist, cur->value);
    }
}

void ListNode_swap(ListNode *a, ListNode *b)
{
    void *temp = a->value;
    a->value = b->value;
    b->value = temp;
}
