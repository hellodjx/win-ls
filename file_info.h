#ifndef __FILE_INFO_H__
#define __FILE_INFO_H__

#include <stdio.h>
#include <io.h>
#include <string.h>
//#include <dir.h>
#include <time.h>
#include "list.h"
#include "dbg.h"


#define MAX_FILENAME_LEN 40	                            //可显示的文件名最大长度
#define MAX_FILES_NUM 1000	                            //最大查找文件数量
#define MAX_IGNORE_SETTING_NUM 3                        //设置忽略选项的最大数量，基本没用

#define SHORT_FORMAT 0                             
#define LONG_LIST_FORMAT 1

#define SORT_BY_REVERSAL_FILENAME 0                     //按文件名倒序
#define SORT_BY_LAST_EDIT_TIME 1                        //按最后修改时间排序
#define SORT_BY_DEFAULT 2                               //默认排序方式

//定义枚举类型IgnoreSetting，用以设置不显示哪种文件的选项
typedef enum{
    Ignore_None,                                    //不忽略
    Ignore_Hidden,                                  //忽略隐藏文件
    Ignore_Folder,                                  //忽略文件夹，暂时没用
}IgnoreSetting;

//定义结构体TotalFilesInfo，储存显示选项和用户指定的路径下的文件列表
typedef struct TotalFilesInfo{
    List *files_list;
    short int format;
    short int sort;
    IgnoreSetting ignore_setting[MAX_IGNORE_SETTING_NUM];
}TotalFilesInfo;

//为TotalFilesInfo结构体申请内存
TotalFilesInfo* total_files_info_create();

//初始化文件信息显示选项
int init_total_files_info(TotalFilesInfo *total_files_info);

//设置不显示的文件
void set_ignore_setting(TotalFilesInfo *total_files_info, IgnoreSetting setting);

//取消设置不显示的文件
void unset_ignore_setting(TotalFilesInfo *total_files_info, IgnoreSetting setting);

//查找path路径下的所有文件/文件夹，并存入TotalFilesInfo结构体的files_list中
int find_all_files(TotalFilesInfo *total_files_info, char *path);

//对TotalFilesInfo结构体中的files_list进行冒泡排序，how_to_sort指定采用哪种排序，例如SORT_BY_LAST_EDIT_TIME
int files_list_bubble_sort(List *list, int how_to_sort);

//查看是否已设置某项不显示
int is_set_ignore_setting(TotalFilesInfo *total_files_info, IgnoreSetting setting);

//释放TotalFilesInfo结构体内存
void total_files_info_destory(TotalFilesInfo *total_files_info);

#endif